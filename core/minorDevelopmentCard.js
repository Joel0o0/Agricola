window.minorDevelopmentCards = {
    A010: {
        id: "A010",
        title: "木棚",
        condition: "仍然住在木屋",
        cost: { wood: 2, reed: 1 },
        description: "本卡牌只能通过‘主要发展’行动打出。本卡牌为一个家庭成员提供房间。你不能再翻新了。",
        extraRooms: 1, // 提供一个额外的房间
    },
    A013: {
        id: "A013",
        title: "翻新公司",
        condition: "住在正好2个房间的木屋",
        cost: { wood: 4 }, 
        description: "当你打出本卡牌时，你立即获得3个泥块。然后，你可以立即翻新，无需支付建造资源。",
    },
    A022: {
        id: "A022",
        title: "电报",
        condition: "供应堆中至少有1个栅栏",
        cost: { food: 2 },
        description: "你供应堆中每有1个栅栏就在目前的轮数上加1，并标记对应的轮次格。仅在该轮内，你可以从你的供应堆中放置一个家庭成员。",
        scores: 1,
    },
    A033: {
        id: "A033",
        title: "大农庄",
        condition: "所有农场格子都已被使用",
        cost: {},
        description: "当你打出本卡牌时，距离游戏结束每有1个未进行的完整轮次，立即获得1点奖励分数和2个食物。",
    },
    A034: {
        id: "A034",
        title: "长柄修枝剪",
        condition: "2张职业卡牌",
        cost: { wood: 1 },
        description: "每当你建造1个或更多的栅栏时，你还可以使用本卡牌，来用你供应堆中的1个木材和1个栅栏交换2个食物和1点奖励分数。",
    },
    A036: {
        id: "A036",
        title: "外观雕刻",
        condition: "你供应堆的木材大于等于目前的轮数",
        cost: { clay: 2 },
        description: "当你打出本卡牌时，你可以用任意数量的食物交换奖励分数，每个食物交换1点奖励分数，最多不超过已完成的收成次数。",
    },
    A049: {
        id: "A049",
        title: "巢址",
        condition: "1张职业卡牌",
        cost: { food: 1 },
        description: "每当1个芦苇在准备阶段被放置在1个非空的‘芦苇池’累积格时，你获得1个食物。",
    },
}

cardFunc.A013Condition = function () {
    const roomType = window.myData.roomType;
    const totalRooms = countOfRooms();

    if (roomType !== "woodenHut" || totalRooms !== 2) {
        alert("打出‘翻新公司’的条件未满足：住在正好2个房间的木屋");
        return false;
    }
    return true;
};

cardFunc.A013Action = function () {
    window.myData.clay += 3;

    // 先将需要扣除的资源加回到玩家的资源中
    const totalWoodenHuts = window.myData.farmCells.flat().filter(cell => cell.building === 'woodenHut').length;
    window.myData.reed += 1;
    window.myData.clay += totalWoodenHuts;

    handleHouseRenovation();
};

cardFunc.A022Condition = function () {
    if (window.myData.leftFences < 1) {
        alert("打出‘电报’的条件未满足：供应堆中至少有1个栅栏");
        return false;
    }
    return true;
};

cardFunc.A022Action = function () {
    const card = window.myData.minorDevelopmentCards.find(card => card.id === 'A022');
    card.roundNum = window.boardData.currentRound + window.myData.leftFences;
};

function canPlaceWorkerForA022() {
    const card = window.myData.minorDevelopmentCards.find(card => card.id === 'A022');
    return card && card.roundNum === window.boardData.currentRound && window.myData.totalWorkers < 5;
}

cardFunc.A022EventHandler = function () {
    eventBus.addEventListener(EVENT_START_NEW_ROUND, (event) => {
        if (canPlaceWorkerForA022()) {
            window.myData.unusedWorker += 1;
            updateResourceBar();
            alert('由于“电报”次要发展卡，你可以在本轮多放置一个工人。');
        }
    });
};

cardFunc.A033Action = function () {
    // 计算未进行的完整轮次数
    const remainingRounds = 14 - window.boardData.currentRound;

    // 获得奖励分数和食物
    window.myData.score += remainingRounds;
    window.myData.food += 2 * remainingRounds;

    updateResourceBar();
};

cardFunc.A033Condition = function A033Condition() {
    const unusedFarmCells = window.myData.farmCells
        .flat()
        .filter((cell) => {
            // 筛选出未使用的农场格子
            return (
                cell.building === null &&
                cell.pastureIndex === null
            );
        });


    if (unusedFarmCells.length !== 0) {
        alert("打出‘大农庄’的条件未满足：所有农场格子都已被使用");
        return false;
    }

    return true;
};

cardFunc.A034Condition = function () {
    if (window.myData.occupationCards.length != 2) {
        alert("打出‘长柄修枝剪’的条件未满足：需要2张职业卡牌");
        return false;
    }
    return true;
};

cardFunc.A034EventHandler = function () {
    eventBus.addEventListener(EVENT_FENCE_BUILDING_COMPLETED, function (event) {
        const shouldApplyEffect = confirm("你拥有‘长柄修枝剪’卡牌。是否使用1个木材和1个栅栏交换2个食物和1点奖励分数？");
        if (shouldApplyEffect) {
            if (window.myData.wood >= 1 && window.myData.leftFences >= 1) {
                window.myData.wood -= 1;
                window.myData.leftFences -= 1;
                window.myData.food += 2;
                window.myData.score += 1;
                updateResourceBar();
            } else {
                alert("资源不足，无法执行‘长柄修枝剪’的效果。");
            }
        }
    });
};

cardFunc.A036Condition = function() {
    if (window.myData.wood < window.boardData.currentRound) {
        alert("打出‘外观雕刻’的条件未满足：你供应堆的木材大于等于目前的轮数");
        return false;
    }
    return true;
}

cardFunc.A036Action = function() {
    const harvestTimes = getHarvestTimes(window.boardData.currentRound);
    const maxFoodToExchange = harvestTimes;
    let isValidInput = false;

    do {
        const foodToExchange = prompt(`请输入要交换的食物数量（最大值为${maxFoodToExchange}），点击取消则不兑换：`);
        if (foodToExchange === null || foodToExchange === '') {
            break;
        }
        const foodToExchangeNum = parseInt(foodToExchange);
        if (isNaN(foodToExchangeNum) || foodToExchangeNum <= 0 || foodToExchangeNum > maxFoodToExchange) {
            alert(`请输入一个有效的数字（最大值为${maxFoodToExchange}）！`);
            continue;
        }

        if (!confirm(`确定要用${foodToExchangeNum}个食物交换${foodToExchangeNum}点奖励分数吗？`)) {
            continue;
        }

        // 扣除资源
        deductResources({ food: foodToExchangeNum });

        // 增加奖励分数
        window.myData.score += foodToExchangeNum;

        // 立即刷新UI展示变更的效果
        updateResourceBar();
        updateFarm();

        isValidInput = true;
    } while (!isValidInput);
};

cardFunc.A049Condition = function() {
    if (window.myData.occupationCards.length != 1) {
        alert("打出‘巢址’的条件未满足：1张职业卡牌");
        return false;
    }
    return true;
};

cardFunc.A049EventHandler = function() {
    eventBus.addEventListener(EVENT_RESOURCE_GROWTH, function(event) {
        const workSpaceKey = event.detail.workSpace;
        const amountBeforeIncrease = event.detail.amountBeforeIncrease; // 获取增长之前的芦苇的数量
        if (workSpaceKey === 'reed-bank' && amountBeforeIncrease > 0) {
            const newFood = window.boardData[workSpaceKey] - amountBeforeIncrease;
            window.myData.food += newFood;
            alert(`由于‘巢址’的效果，你将额外获得${newFood}个食物`);
            updateResourceBar();
        }
    });
};