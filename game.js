
function travelingPlayersFoodIncrease() {
    if (window.boardData.travelingPlayersFood === undefined) {
        return;
    }
    window.boardData.travelingPlayersFood += 1;
    var p = $("#traveling-players p");
    p.text("+1食物>>>" + window.boardData.travelingPlayersFood);
}

function coppiceIncrease() {
    if (window.boardData.coppice === undefined) {
        return;
    }
    window.boardData.coppice += 1;
    var p = $("#coppice p");
    p.text("+1木>>>" + window.boardData.coppice);
}

function groveIncrease() {
    if (window.boardData.grove === undefined) {
        return;
    }
    window.boardData.grove += 2;
    var p = $("#grove p");
    p.text("+2木>>>" + window.boardData.grove);
}

function clayPit2Increase() {
    if (window.boardData.clayPit2 === undefined) {
        return;
    }
    window.boardData.clayPit2 += 2;
    var p = $("#clay-pit2 p");
    p.text("+2泥>>>" + window.boardData.clayPit2);
}
/* 职业卡
A101: {
    id: "A101",
    title: "炊具商",
    description: "游戏结束计分时，你每拥有一个锅图标的发展卡，你获得1点奖励分数。",
    type: "A",
    suitable: 1,
    },*/
/* 职业卡
    A168: {
        id: "A168",
        title: "动物老师",
        description: "每当你使用一个“职业训练”行动格后，你还可以立即用0/1/2个食物购买1个羊/野猪/牛。",
        type: "A",
        suitable: 4,
    },*/

