window.occupationCards = {
    A090: {
        id: "A090",
        title: "驭犁者",
        description: "一旦你住进石屋，在每轮开始时，你可以支付1个食物来犁1块田。",
    },
    A095: {
        id: "A095",
        title: "垂钓者",
        description: "每当你使用一个有至多2个食物的“钓鱼”累积格后，你获得一个“主要或次要发展”行动。",
    },
    A098: {
        id: "A098",
        title: "畜棚建筑师",
        description: "游戏结束计分时，你农场上每有1个牧场外的畜棚，获得1点奖励分数。",
    },
    A100: {
        id: "A100",
        title: "管理者",
        description: "在每轮的回家阶段，如果你有至少3个家庭成员从累积格中回家，你可以用1个食物购买1点奖励分数。",
    },
    A106: {
        id: "A106",
        title: "泥浆传播者",
        description: "在每次收成的收割步骤，每当你从农田拿取最后的谷物/蔬菜时，你还获得2个食物/1个食物。",
    },
    A108: {
        id: "A108",
        title: "采菇人",
        description: "你每次使用一个木材累积格之后，可以立即用1个木材交换2个食物。如果你这么做，将用于交换的木材放在积累格上。",
    },
    A110: {
        id: "A110",
        title: "泥灰匠",
        description: "你每次建造至少1个砖房或将房屋从砖房翻新为石屋时，还获得3个食物。",
    },
    A111: {
        id: "A111",
        title: "泥水师傅",
        description: "你每次建造至少1个房间时，你可以在接下来4轮的轮次格上各放置1个食物。在这些轮开始时，你从该轮格子上拿取放置的食物。",
    },
    A112: {
        id: "A112",
        title: "小麦收割工",
        description: "当你打出本卡牌时，立即获得1个谷物。在每次收成的收割步骤中，你可以从你的每个谷物农田上额外收获1个谷物。",
    },
    A114: {
        id: "A114",
        title: "季节工",
        description: "你每次使用‘临时工’行动格时，额外获得1个谷物。从第6轮开始，你可以改为额外获得1个蔬菜。",
    },
    A116: {
        id: "A116",
        title: "伐木工",
        description: "你每次使用一个木材累积格时，额外获得1个木材。",
    },
    A117: {
        id: "A117",
        title: "背木工",
        description: "当你打出本卡牌时，你面前每有一个发展，你立刻获得1个木材。",
    },
    A118: {
        id: "A118",
        title: "树木园丁",
        description: "在每次收成的收割步骤，你获得1个木材，且你可以用食物购买至多额外2个木材，每个食物购买1个木材。",
    },
    A119: {
        id: "A119",
        title: "拾木者",
        description: "你每次使用“犁田”、“谷物种子”、“运用谷物”或“耕种”行动格之后，立即获得1个木材。",
    },
    A121: {
        id: "A121",
        title: "黏土穿孔者",
        description: "当你打出本卡牌时和每当你使用一个“职业训练”行动格或“黏土坑”累积格后，你获得1个泥块。",
    },
    A126: {
        id: "A126",
        title: "熟练工",
        description: "每当你使用第1、2、3、4轮的轮次格上的行动格卡牌前，你获得1个木材/泥块/芦苇/石材。",
    },
}

cardFunc.A090EventHandler = function () {
    eventBus.addEventListener(EVENT_START_NEW_ROUND, function (event) {
        if (window.myData.roomType === "stoneHouse" && confirm("驭犁者，你是否支付1个食物来犁1块田？")) {
            if (window.myData.food >= 1) {
                window.myData.food -= 1;
                updateResourceBar();
                startPlowingFields();
            } else {
                alert("你的食物不足，无法犁田。");
            }
        }
    });
};

cardFunc.A095EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, function (event) {
        const { workSpaceKey, amountBeforeUse } = event.detail;
        if (workSpaceKey === 'fishing' && amountBeforeUse <= 2) {
            if (confirm("垂钓者，你是否要进行一个“主要或次要发展”行动？")) {
                // 允许玩家选择一个主要或次要发展卡
                selectDevelopmentCard("developmentCardAction");
            }
        }
    });
};

cardFunc.A098FinalScore = function() {
    const outsideStables = window.myData.farmCells.flat().filter(cell => cell.building === 'stable' && cell.pastureIndex === null).length;
    return outsideStables;
};

cardFunc.A100EventHandler = function () {
    eventBus.addEventListener(EVENT_AFTER_GO_HOME, function (event) {
        const { workersWorkspaces } = event.detail;

        // 计算从积累格中回家的工人的数量
        const accumulationSpotsCount = workersWorkspaces.filter(workspaceId => {
            return resourceMappings[workspaceId] && resourceMappings[workspaceId].increaseStep > 0;
        }).length;

        if (accumulationSpotsCount >= 3 && window.myData.food >= 1 && confirm("管理者，你是否支付1个食物来获得1点奖励分数？")) {
            window.myData.food -= 1;
            window.myData.score += 1;
            updateResourceBar();
        }
    });
};

cardFunc.A106EventHandler = function () {
    eventBus.addEventListener(EVENT_HARVEST_FOR_CELL, function (event) {
        const { cell, resourceType } = event.detail;
        var deltaFood = 0;
        if (resourceType === 'grain' && cell.grain === 0) {
            deltaFood = 2;
        } else if (resourceType === 'vegetable' && cell.vegetable === 0) {
            deltaFood = 1;
        }
        if (deltaFood > 0) {
            window.myData.food += deltaFood;
            alert(`泥浆传播者，你在农田(${cell.x}, ${cell.y})上额外获得${deltaFood}个食物。`);
        }
    });
};

cardFunc.A108EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, function (event) {
        const { resourceType } = event.detail;
        // 检查是否为木材累积格
        if (resourceType === 'wood') {
            if (confirm("采菇人，你是否要用1个木材交换2个食物？")) {
                window.myData.wood -= 1; 
                window.myData.food += 2; 
                updateResourceBar();

                // 将用于交换的木材放在积累格上
                const workSpaceKey = event.detail.workSpaceKey;
                window.boardData[workSpaceKey] += 1; // 增加累积格上的木材数量
                updateAccumulationResource(workSpaceKey); // 更新累积格的显示
            }
        }
    });
};

cardFunc.A110EventHandler = function () {
    eventBus.addEventListener(EVENT_AFTER_BUILD_HOUSE, function (event) {
        const { roomType, newRoomCount } = event.detail;
        if (roomType === "clayHut" && newRoomCount > 0) {
            window.myData.food += 3;
            updateResourceBar();
            alert("泥灰匠，由于你建造了至少1个砖房，获得3个食物。");
        }
    });

    eventBus.addEventListener(EVENT_AFTER_HOUSE_RENOVATION, function (event) {
        const { to } = event.detail;
        if (to === "stoneHouse") {
            window.myData.food += 3;
            updateResourceBar();
            alert("泥灰匠，由于你将房屋从砖房翻新为石屋，获得3个食物。");
        }
    });
};

cardFunc.A111EventHandler = function () {
    eventBus.addEventListener(EVENT_AFTER_BUILD_HOUSE, function (event) {
        const { newRoomCount } = event.detail;
        if (newRoomCount > 0) {
            var resultRounds = operateRoundRewardsForNextXRounds(4, (roundIndex) => {
                const oldValue = window.myData.roundRewards[roundIndex].food || 0;
                window.myData.roundRewards[roundIndex].food = oldValue + 1;
            });
            if (resultRounds > 0) {
                alert(`水泥师傅，由于你建造了房屋，接下来${resultRounds}个回合会分别额外获得1食物。`);
            }
        }
    });
};

cardFunc.A112Action = function () {
    window.myData.grain += 1;
    updateResourceBar();
    alert("小麦收割工，你立即获得1个谷物。");
};

cardFunc.A112EventHandler = function () {
    eventBus.addEventListener(EVENT_HARVEST_FOR_CELL, function(event) {
        const { cell, resourceType } = event.detail;
        if (resourceType === 'grain') {
            window.myData.grain += 1;
            alert(`小麦收割工，你从农田(${cell.x}, ${cell.y})额外收获1个谷物。`);
            updateFarm();
            updateResourceBar();
        }
    });
};

cardFunc.A114EventHandler = function() {
    eventBus.addEventListener(EVENT_USE_RESOURCE, function (event) {
        const { workSpaceKey } = event.detail;
        if (workSpaceKey != "day-laborer") {
            return;
        }
        const turnNumber = window.boardData.currentRound;
        if (turnNumber < 6) {
            window.myData.grain += 1;
            alert('季节工，你额外获得1个小麦。');
        } else {
            if (confirm('季节工，你是否从”额外获得1个谷物“改为”额外获得1个蔬菜“。')) {
                window.myData.vegetable += 1;
                alert('季节工，你额外获得1个蔬菜。');
            } else {
                window.myData.grain += 1;
                alert('季节工，你额外获得1个小麦。');
            }
        }
        updateResourceBar();
    });
};

cardFunc.A116EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, function (event) {
        const { resourceType } = event.detail;
        if (resourceType === 'wood') {
            window.myData.wood += 1;
            updateResourceBar();
            customAlert('伐木工，你额外获得1个木材。');
        }
    });
};

cardFunc.A117Action = function () {
    const majorDevelopments = window.myData.developmentCards.length;
    const minorDevelopments = window.myData.minorDevelopmentCards.length;
    const totalDevelopments = majorDevelopments + minorDevelopments;

    if (totalDevelopments > 0) {
        window.myData.wood += totalDevelopments;
        updateResourceBar();
        customAlert(`背木工，由于已打出的发展卡，你获得了${totalDevelopments}个木材。`);
    }
};

cardFunc.A118EventHandler = function () {
    allPromptViews["A118"] = {
        class: ".A118-prompt-view",
    };
    eventBus.addEventListener(EVENT_HARVEST_AFTER_REAP, async function (event) {
        window.myData.wood += 1;
        updateResourceBar();

        await showA118PromptView();
    });
};

async function showA118PromptView() {
    await pushStatusAndWaitNewStatus(function () {
        window.actionData.status = "A118";
        window.actionData.substatus = [];
        updateAdditionalView();
    });
}

function purchaseWoodForA118(numAdditionalWood) {
    const requiredFood = numAdditionalWood;
    if (window.myData.food < requiredFood) {
        customAlert("你的食物不足，无法购买更多木材。");
        return;
    }

    if (numAdditionalWood == 0) {
        customAlert(`你选择了不购买额外木材。`);
    } else {
        window.myData.food -= requiredFood;
        window.myData.wood += numAdditionalWood;
        customAlert(`你使用${requiredFood}个食物购买了${numAdditionalWood}个木材。`);
        updateResourceBar();
    }
        
    popStatusOrEndAction();
}

cardFunc.A119EventHandler = function () {
    eventBus.addEventListener(EVENT_PLOW_FIELD, function (event) {
        window.myData.wood += 1;
        updateResourceBar();
        customAlert('拾木者，犁田使你额外获得1个木材。');
    });

    eventBus.addEventListener(EVENT_GRAIN_UTILIZATION, function (event) {
        window.myData.wood += 1;
        updateResourceBar();
        customAlert('拾木者，运用谷物使你额外获得1个木材。');
    });

    eventBus.addEventListener(EVENT_USE_RESOURCE, function (event) {
        if (event.detail.workSpaceKey === 'grain-seed') {
            window.myData.wood += 1;
            updateResourceBar();
            customAlert('拾木者，谷物种子行动使你额外获得1个木材。');
        }
    });

    eventBus.addEventListener(EVENT_FARMING, function (event) {
        window.myData.wood += 1;
        updateResourceBar();
        customAlert('拾木者，耕种使你额外获得1个木材。');
    });
};

cardFunc.A121EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_OCCUPATION_TRAINING, function (event) {
        window.myData.clay += 1;
        updateResourceBar();
        customAlert('黏土穿孔者，职业训练使你获得1个泥块。');
    });

    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, function (event) {
        const { workSpaceKey } = event.detail;
        if (workSpaceKey === 'clay-pit') {
            window.myData.clay += 1;
            updateResourceBar();
            customAlert('黏土穿孔者，使用黏土坑使你额外获得1个泥块。');
        }
    });
};

cardFunc.A121Action = function () {
    window.myData.clay += 1;
    updateResourceBar();
    customAlert("黏土穿孔者，打出此牌使你获得1个泥块。");
};

cardFunc.A126EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_TURN_ACTION_BEFORE, async function (event) {
        const { workSpaceKey } = event.detail;
        if (turnCardsIdList.slice(0, 4).includes(workSpaceKey) == false) {
            return; // 如果使用的轮次牌不是1-4轮的，则不触发奖励
        }
        const resources = ["wood", "clay", "reed", "stone"];
        const roundIndex = turnCardsIdList.indexOf(workSpaceKey) + 1;
        const chosenResource = await showResourceSelectionDialog(`熟练工, 由于你选择使用第${roundIndex}轮的轮次牌，可以选择一种资源来获得`, resources);
        window.myData[chosenResource] += 1;
        updateResourceBar();
        customAlert(`熟练工，你获得了1个${TypeFullName[chosenResource]}。`);
    });
};
