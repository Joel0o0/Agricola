window.minorDevelopmentCards.A073 = {
    id: "A073",
    title: "农业化肥",
    condition: "1个牧场",
    cost: {},
    description: "每当你在一个行动中将至少2个未使用的格子转变为已使用后，你额外获得一次‘播种’行动。",
    type: "A",
};

cardFunc.A073Condition = function() {
    if (window.myData.pastures.length != 1) {
        customAlert("打出'农业化肥'的条件未满足：1个牧场");
        return false;
    }
    return true;
};

cardFunc.A073EventHandler = function() {
    eventBus.addEventListener(EVENT_FENCE_BUILDING_COMPLETED, handleA073Effect);
    eventBus.addEventListener(EVENT_AFTER_BUILD_HOUSE, handleA073Effect);
};

async function handleA073Effect(event) {
    const { usedCellCount } = event.detail;
    if (usedCellCount >= 2) {
        customAlert("由于'农业化肥'的效果，你可以额外进行一次播种行动。");
        await startSowingSeeds();
    }
}


