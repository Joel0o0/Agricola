window.occupationCards.A092 = {
    id: "A092",
    title: "养父母",
    description: "你可以支付1个食物，让本轮新增的后代执行一个行动。如果你这么做，该新增的家庭成员不再视作“新生儿”。",
};

cardFunc.A092EventHandler = function () {
    eventBus.addEventListener(EVENT_AFTER_ADD_FAMILY_MEMBER, function (event) {
        if (confirm("养父母，你是否支付1个食物，让本轮新增的后代执行一个行动？")) {
            if (window.myData.food >= 1) {
                window.myData.food -= 1;
                // 将新增加的家庭成员不再视作“新生儿”
                window.myData.newborns.pop();
                window.myData.unusedWorker += 1; // 新增的家庭成员可以执行一个额外的行动
                updateResourceBar();
            } else {
                alert("你的食物不足，无法让新生儿长大。");
            }
        }
    });
}