var itemScore = [
    {
        title: "农田",
        scoreFunc: function () {
            const fields = window.myData.farmCells.flat().filter(cell => cell.building === 'field').length;
            if (fields <= 1) return -1;
            else if (fields <= 4) return fields - 1;
            else return 4;
        },
    },
    {
        title: "牧场",
        scoreFunc: function () {
            const pastures = window.myData.pastures.length;
            if (pastures <= 0) return -1;
            else if (pastures <= 3) return pastures;
            else return 4;
        },
    },
    {
        title: "小麦",
        scoreFunc: function () {
            var grain = window.myData.grain;
            grain += window.myData.farmCells.flat().reduce((sum, cell) => sum + (cell.building === 'field' ? cell.grain : 0), 0);
            if (grain <= 0) return -1;
            else if (grain <= 3) return 1;
            else if (grain <= 5) return 2;
            else if (grain <= 7) return 3;
            else return 4;
        },
    },
    {
        title: "蔬菜",
        scoreFunc: function () {
            var vegetables = window.myData.vegetable;
            vegetables += window.myData.farmCells.flat().reduce((sum, cell) => sum + (cell.building === 'field' ? cell.vegetable : 0), 0);
            if (vegetables <= 0) return -1;
            else if (vegetables <= 3) return vegetables;
            else return 4;
        },
    },
    {
        title: "羊",
        scoreFunc: function () {
            const sheep = window.myData.sheep;
            if (sheep <= 0) return -1;
            else if (sheep <= 3) return 1;
            else if (sheep <= 5) return 2;
            else if (sheep <= 7) return 3;
            else return 4;
        },
    },
    {
        title: "猪",
        scoreFunc: function () {
            const pig = window.myData.pig;
            if (pig <= 0) return -1;
            else if (pig <= 2) return 1;
            else if (pig <= 4) return 2;
            else if (pig <= 6) return 3;
            else return 4;
        },
    },
    {
        title: "牛",
        scoreFunc: function () {
            const cattle = window.myData.cattle;
            if (cattle <= 0) return -1;
            else if (cattle === 1) return 1;
            else if (cattle <= 3) return 2;
            else if (cattle <= 5) return 3;
            else return 4;
        },
    },
    {
        title: "空地",
        scoreFunc: function () {
            const emptyLands = window.myData.farmCells.flat().filter(cell => 
                !cell.building && cell.pastureIndex === null
            ).length;
            return -emptyLands;
        },
    },
    {
        title: "畜棚",
        scoreFunc: function () {
            const stables = window.myData.farmCells.flat().filter(cell => cell.building === 'stable' && cell.pastureIndex !== null).length;
            return stables;
        },
    },
    {
        title: "房屋",
        scoreFunc: function () {
            const cells = window.myData.farmCells.flat();
            const clayHuts = cells.filter(cell => cell.building === 'clayHut').length;
            const stoneHouses = cells.filter(cell => cell.building === 'stoneHouse').length;
            return clayHuts + 2 * stoneHouses;
        },
    },
    {
        title: "工人",
        scoreFunc: function () {
            return window.myData.totalWorkers * 3;
        },
    },
    {
        title: "主要发展卡",
        scoreFunc: function () {
            const developmentScores = {
                fireplace: 1,
                cookingHearth: 1,
                stoneOven: 3,
                brickOven: 2,
                potteryWorkshop: 2,
                basketryWorkshop: 2,
                carpentryWorkshop: 2,
                well: 4
            };
        
            return window.myData.developmentCards.reduce((total, card) => {
                return total + (developmentScores[card.type] || 0);
            }, 0);
        },
    },
    {
        title: "次要发展卡",
        scoreFunc: function () {        
            return window.myData.minorDevelopmentCards.reduce((total, card) => {
                return total + (card.scores || 0);
            }, 0);
        },
    },
    {
        title: "已获得分数",
        scoreFunc: function () {
            return window.myData.score;
        },
    },
    {
        title: "劣势",
        scoreFunc: function () {
            return -window.myData.disadvantages;
        },
    },
];

function calculateFinalScore() {

    var scores = itemScore.map(item => ({
        title: item.title,
        score: item.scoreFunc()
    }));

    window.myData.occupationCards.forEach(card => {
        const scoreFunc = cardFunc[`${card.id}FinalScore`];
        if (scoreFunc) {
            scores.push({
                title: card.title,
                score: scoreFunc()
            });
        }
    });

    const totalScore = scores.reduce((sum, { score }) => sum + score, 0);

    scores.push({
        title: "总计",
        score: totalScore
    });

    return scores;
}

function displayFinalScore() {
    const scores = calculateFinalScore();

    // 更新视图
    const scoreHtml = scores.map(({ title, score }) => `<p>${title}: ${score}分</p>`).join("");

    $(".final-score-view .item-scores").html(scoreHtml);

    window.actionData.status = "finalScore";

    updateAdditionalView();
    // 隐藏其他控制按钮
    $(".control-panel").css("display", "none");
    
}

async function beforeFinalScore() {
    window.actionData.status = "beforeFinalScore";
    updateAdditionalView();

    return new Promise((resolve) => {
        window.resolveBeforeFinalScore = resolve; // 保存 resolve 函数，以便在用户点击完成按钮时调用
    });
}

function completeBeforeFinalScore() {
    endAction();

    if (window.resolveBeforeFinalScore) {
        window.resolveBeforeFinalScore();
        window.resolveBeforeFinalScore = null;
    }
}