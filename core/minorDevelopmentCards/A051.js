window.minorDevelopmentCards.A051 = {
    id: "A051",
    title: "漂网渔船",
    condition: "",
    cost: { wood: 1, reed: 1 },
    description: "每当你使用“钓鱼”积累格时，你额外获得2个食物",
    type: "A",
    scores: 1,
}

cardFunc.A051EventHandler = function () {
    // 漂网渔船的逻辑是在使用“钓鱼”积累格时，额外获得2个食物
    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, (event) => {
        const { workSpaceKey } = event.detail;
        if (workSpaceKey === 'fishing') {
            window.myData.food += 2;
            updateResourceBar();
            customAlert('由于“漂网渔船”次要发展卡，你额外获得了2个食物。');
        }
    });
};