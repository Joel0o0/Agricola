window.occupationCards.A091 = {
    id: "A091",
    title: "轮换耕种者",
    description: "每当你使用1个木材累积格时，你还可以支付3个食物来犁1块田。",
};

cardFunc.A091EventHandler = function () {
    eventBus.addEventListener(EVENT_USE_RESOURCE_ACCUMULATION, function (event) {
        const { resourceType } = event.detail;
        if (resourceType === 'wood' && confirm("轮换耕种者，你是否支付3个食物来犁1块田？")) {
            if (window.myData.food >= 3) {
                window.myData.food -= 3;
                updateResourceBar();
                startPlowingFields();
            } else {
                alert("你的食物不足，无法犁田。");
            }
        } 
    });
};