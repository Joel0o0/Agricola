window.minorDevelopmentCards.A054 = {
    id: "A054",
    title: "信贷",
    condition: "最多3张职业卡牌",
    cost: {},
    description: "当你打出本卡牌时，你立刻获得5个食物。在每个不以收成结束的轮次结束时，你必须支付1个食物，或者获得1劣势。",
    type: "A",
};

cardFunc.A054Condition = function() {
    if (window.myData.occupationCards.length > 3) {
        alert("打出'信贷'的条件未满足:最多3张职业卡牌");
        return false;
    }
    return true;
};

cardFunc.A054Action = function() {
    window.myData.food += 5;
    updateResourceBar();
    customAlert("由于打出’信贷‘，你立刻获得5个食物。");
};

cardFunc.A054EventHandler = function() {
    eventBus.addEventListener(EVENT_CURRENT_ROUND_END, async function(event) {
        const currentRound = window.boardData.currentRound;
        if (!isHarvestRound(currentRound)) { // 检查当前轮次是否不是收获轮
            if (window.myData.food >= 1) {
                var isConfirmed = await customConfirm("由于'信贷'卡的效果，你需要支付1个食物，否则获得1劣势。是否支付?");
                if (isConfirmed) {
                    window.myData.food -= 1;
                } else {
                    window.myData.disadvantages += 1;
                    await customAlert("你选择不支付食物，获得1个劣势。");
                }
            } else {
                window.myData.disadvantages += 1;
                await customAlert("由于'信贷'卡的效果，你没有足够的食物支付，获得1个劣势。");
            }
            updateResourceBar();
        }
    });
};