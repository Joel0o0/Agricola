//------------- 建造房屋和畜棚
function switchToBuildingStables() {
    window.actionData.substatus = ["buildStables"];
    updateAdditionalView();
}

function switchToBuildingRooms() {
    window.actionData.substatus = [];
    updateAdditionalView();
}

function updatePromptViewForBuildingRoomsOrStables() {
    var status = window.actionData.status;
    var pText = "";
    if (status === "buildRooms" && hasSubstatus("buildStables")) {
        $(".build-rooms-prompt-view .build-stables").css("display", "none");
        $(".build-rooms-prompt-view .build-rooms").css("display", "block");
        pText = '2木=1畜棚<br />点击农场中的格子来建造1间畜棚<br />点击建造房屋按钮则切换为房屋建造状态';
        
    } else if (status === "buildRooms") {
        $(".build-rooms-prompt-view .build-stables").css("display", "block");
        $(".build-rooms-prompt-view .build-rooms").css("display", "none");
        pText = '5木+2芦=1木屋<br />5泥+2芦=1砖屋<br />5石+2芦=1石屋<br />点击农场中的格子来建造1间房屋<br />点击建造畜棚按钮则切换为畜棚建造状态';
    }
    var promptView = $('.build-rooms-prompt-view');
    promptView.find('p:first').html(pText);
}

function selectBuildRooms() {
    if (!confirm('确定选择"农场扩建"？')) {
        return;
    }

    window.actionData.status = "buildRooms";
    window.actionData.newRoomCount = 0;

    // 初始化当前行动已使用的格子数
    window.actionData.usedCells = 0;

    updateAdditionalView();

    window.myData.unusedWorker -= 1;
    updateResourceBar();

    appendWorkerToTitle("build-rooms");

}

function endBuildRooms() {
    endAction();

    dispatchEventDelay(EVENT_AFTER_BUILD_HOUSE, {
        roomType: window.myData.roomType,
        newRoomCount: window.actionData.newRoomCount,
        usedCellCount: window.actionData.usedCells
    });
    window.actionData.newRoomCount = 0;
}


//------------- 房屋改造
function handleHouseRenovation() {
    const myData = window.myData;
    const totalWoodenHuts = myData.farmCells.flat().filter(cell => cell.building === "woodenHut").length;
    const totalClayHuts = myData.farmCells.flat().filter(cell => cell.building === "clayHut").length;
    if (totalWoodenHuts <= 0 && totalClayHuts <= 0) {
        alert('没有可以改建的房屋。');
        return false;
    }

    const requiredReed = 1;

    if (totalWoodenHuts > 0) {
        const requiredClay = totalWoodenHuts;

        if (myData.reed < requiredReed || myData.clay < requiredClay) {
            alert('资源不足，无法改建所有木屋为砖房。');
            return false;
        }

        // 扣除资源
        myData.reed -= requiredReed;
        myData.clay -= requiredClay;

        // 改建房屋
        myData.farmCells.forEach(column => {
            column.forEach(cell => {
                if (cell.building === "woodenHut") {
                    cell.building = "clayHut";
                }
            });
        });
        myData.roomType = "clayHut";

        dispatchEventDelay(EVENT_AFTER_HOUSE_RENOVATION, { from: "woodenHut", to: "clayHut" });
    } else if (totalClayHuts > 0) {
        const requiredStone = totalClayHuts;

        if (myData.reed < requiredReed || myData.stone < requiredStone) {
            alert('资源不足，无法改建所有砖房为石屋。');
            return false;
        }

        // 扣除资源
        myData.reed -= requiredReed;
        myData.stone -= requiredStone;

        // 改建房屋
        myData.farmCells.forEach(column => {
            column.forEach(cell => {
                if (cell.building === "clayHut") {
                    cell.building = "stoneHouse";
                }
            });
        });

        myData.roomType = "stoneHouse";

        dispatchEventDelay(EVENT_AFTER_HOUSE_RENOVATION, { from: "clayHut", to: "stoneHouse" });
    }

    updateFarm();
    updateResourceBar();

    return true;
}

function selectHouseRenovation() {
    if (!dispatchEvent(EVENT_BEFORE_HOUSE_RENOVATION, {}, true)) {
        return;
    }

    if (!confirm('确定选择"房屋改建"？')) {
        return;
    }
    
    if (handleHouseRenovation() == false) {
        return;
    }

    myData.unusedWorker -= 1;
    appendWorkerToTitle("house-renovation");

    // 允许用户打出主要或次要发展卡
    selectDevelopmentCard("developmentCardAction");;
}


//------------- 农场改建
function selectFarmRenovation() {
    if (!dispatchEvent(EVENT_BEFORE_HOUSE_RENOVATION, {}, true)) {
        return;
    }

    if (!confirm('确定选择"农场改建"？')) {
        return;
    }

    if (handleHouseRenovation() == false) {
        return;
    }

    window.actionData.status = "farmRenovation";
    initForBuildingFences();

    // 初始化当前行动已使用的格子数
    window.actionData.usedCells = 0;

    updateAdditionalView();
    window.myData.unusedWorker -= 1;
    updateResourceBar();
    appendWorkerToTitle("farm-renovation");
}

function endFarmRenovation() {
    startAnotherPasture();
    
    endAction();

    dispatchEventFenceBuildingCompleted();
}

// function startHouseRenovation() {
//     if (!confirm('确定选择"房屋翻新"？')) {
//         return;
//     }

//     handleHouseRenovation();
// }
